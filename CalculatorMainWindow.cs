﻿using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using System;

public class CalculatorMainWindow 
{
	public WindowsDriver<WindowsElement> Session { get; }

	public CalculatorMainWindow(WindowsDriver<WindowsElement> session)
	{
		Session = session;
	}

	private static WindowsElement header;

	public void OpenStandardCalculator()
	{
		try
		{
			header = Session.FindElementByAccessibilityId("Header");
		}
		catch
		{
			header = Session.FindElementByAccessibilityId("ContentPresenter");
		}

		// Ensure that calculator is in standard mode
		if (!header.Text.Equals("Standard", StringComparison.OrdinalIgnoreCase))
		{
			Session.FindElementByAccessibilityId("TogglePaneButton").Click();
			var splitViewPane = Session.FindElementByClassName("SplitViewPane");
			splitViewPane.FindElementByName("Standard Calculator").Click();
			Assert.IsTrue(header.Text.Equals("Standard", StringComparison.OrdinalIgnoreCase));
		}		
	}			

	public WindowsElement ClearButton
	{
		get { return Session.FindElementByAccessibilityId("clearButton"); }
	}

	public WindowsElement ResultField
	{
		get { return Session.FindElementByAccessibilityId("CalculatorResults"); }
	}

	public string GetCalculatorResultText()
	{
		return ResultField.Text.Replace("Display is", string.Empty).Trim();
	}

	public void GetClearButton()
    {
		var clearButton = ClearButton;
		clearButton.Click();
	}

	public WindowsElement OneButton
    {
		get { return Session.FindElementByAccessibilityId("num1Button"); }
    }

	public WindowsElement TwoButton
	{
		get { return Session.FindElementByAccessibilityId("num2Button"); }
	}

	public WindowsElement ThreeButton
	{
		get { return Session.FindElementByAccessibilityId("num3Button"); }
	}

	public WindowsElement FourButton
	{
		get { return Session.FindElementByAccessibilityId("num4Button"); }
	}

	public WindowsElement FiveButton
    {
        get { return Session.FindElementByAccessibilityId("num5Button"); }
    }
	
	public WindowsElement SixeButton
    {
        get { return Session.FindElementByAccessibilityId("num6Button"); }
    }
	
	public WindowsElement SevenButton
    {
        get { return Session.FindElementByAccessibilityId("num7Button"); }
    }

	public WindowsElement EightButton
	{
		get { return Session.FindElementByAccessibilityId("num8Button"); }
	}

	public WindowsElement NineButton
	{
        get { return Session.FindElementByAccessibilityId("num9Button"); }
    }

	public WindowsElement ZeroButton
	{
		get { return Session.FindElementByAccessibilityId("num0Button"); }
	}

	public void ClickNumber(char number)
    {
		switch (number)
        {
			case '1':
				OneButton.Click();
				break;
			case '2':
				TwoButton.Click();
				break;
			case '3':
				ThreeButton.Click();
				break;
			case '4':
				FourButton.Click();
				break;
			case '5':
				FiveButton.Click();
				break;
			case '6':
				SixeButton.Click();
				break;
			case '7':
				SevenButton.Click();
				break;
			case '8':
				EightButton.Click();
				break;
			case '9':
				NineButton.Click();
				break;
			case '0':
				ZeroButton.Click();
				break;
        }
    }

	public WindowsElement DivideButton
	{
		get { return Session.FindElementByAccessibilityId("divideButton"); }
    }

	public WindowsElement MultiplyButton
	{
		get { return Session.FindElementByAccessibilityId("multiplyButton"); }
	}

	public WindowsElement MinusButton
	{
		get { return Session.FindElementByAccessibilityId("minusButton"); }
	}

	public WindowsElement PlusButton
	{
		get { return Session.FindElementByAccessibilityId("plusButton"); }
	}

	public void Operators(string Operators)
    {
		switch (Operators)
		{
			case "/":
				DivideButton.Click();
				break;
			case "*":
				MultiplyButton.Click();
				break;
			case "+":
				PlusButton.Click();
				break;
			case "-":
				MinusButton.Click();
				break;
		}
    }

	public WindowsElement EqualButton
	{
		get { return Session.FindElementByAccessibilityId("equalButton"); }	
	}

	public string CalculateResult(string firstNumber, string mathOperator, string secondNumber)
	{		
		for (int i = 0; i < firstNumber.Length; i++)
		{
			var elem = firstNumber[i];
			ClickNumber(elem);
		}
		Operators(mathOperator);
		for (int i = 0; i < secondNumber.Length; i++)
		{
			var elem = secondNumber[i];
			ClickNumber(elem);
		}
		EqualButton.Click();
		return GetCalculatorResultText();
	}    
}
