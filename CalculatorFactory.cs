﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Support.PageObjects;

namespace CalculatorUITest
{
    class CalculatorFactory
    {
        [FindsBy(How = How.Name, Using = "Zero")]
        public IWebElement ZeroButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "One")]
        public IWebElement OneButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Two")]
        public IWebElement TwoButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Three")]
        public IWebElement ThreeButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Four")]
        public IWebElement FourButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Five")]
        public IWebElement FiveButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Six")]
        public IWebElement SixButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Seven")]
        public IWebElement SevenButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Eight")]
        public IWebElement EightButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Nine")]
        public IWebElement NineButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Plus")]
        public IWebElement PlusButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Minus")]
        public IWebElement MinusButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Multiply by")]
        public IWebElement MultiplyButton
        { get; set; }

        [FindsBy(How = How.Name, Using = "Divide by")]
        public IWebElement DivideButton
        { get; set; }

        [FindsBy(How = How.XPath, Using = "//Button[@Name='Equals']")]
        public IWebElement EqualsButton
        { get; set; }
    }
}
