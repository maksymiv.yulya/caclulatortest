﻿using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Support.PageObjects;

namespace CalculatorUITest
{
    [TestFixture]
    public class CalculatorTests
    {        
        private CalculatorMainWindow calc;
        private static WindowsDriver<WindowsElement> _session;

        [OneTimeSetUp]
        public void ClassInitialize()
        {
            _session = CalculatorSession.Start();
            calc = new CalculatorMainWindow(_session);
            calc.OpenStandardCalculator();           
        }

        [OneTimeTearDown]
        public void ClassCleanup()
        {
            if (_session != null)
            {
                _session.Quit();
                _session = null;
            }
        }

        [SetUp]
        public void Clear()
        {
            _session.FindElementByName("Clear").Click();
            Assert.AreEqual("0", calc.GetCalculatorResultText());
        }

        [Test]
        public void EightyEight_Divide_Eleven_Equals_Eight()
        {
            calc.CalculateResult("88", "/", "8");
            Assert.AreEqual("11", calc.GetCalculatorResultText());
        }

        [Test]
        public void Nine_Multiply_Nine_Equals_EightyOne()
        {           
            calc.CalculateResult("9", "*", "9");
            Assert.AreEqual("81", calc.GetCalculatorResultText());
        }

        [Test]
        [TestCase("1", "+", "7", "8")]
        [TestCase("9", "-", "1", "8")]
        [TestCase("8", "/", "8", "1")]
        public void Templatized(string input1, string operation, string input2, string expectedResult)
        {            
            calc.CalculateResult(input1, operation, input2);
            Assert.AreEqual(expectedResult, calc.GetCalculatorResultText());
        }


    }
}
