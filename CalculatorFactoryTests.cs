﻿using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Support.PageObjects;

namespace CalculatorUITest
{
    [TestFixture]
    public class CalculatorFactoryTests
    {        
        private CalculatorMainWindow calc;
        private static WindowsDriver<WindowsElement> _session;

        [OneTimeSetUp]
        public void ClassInitialize()
        {
            _session = CalculatorSession.Start();
            calc = new CalculatorMainWindow(_session);
            calc.OpenStandardCalculator();           
        }

        [OneTimeTearDown]
        public void ClassCleanup()
        {
            if (_session != null)
            {
                _session.Quit();
                _session = null;
            }
        }

        [SetUp]
        public void Clear()
        {
            _session.FindElementByName("Clear").Click();
            Assert.AreEqual("0", calc.GetCalculatorResultText());
        }

        [Test]
        public void One_Plus_Two_Equals_Three()
        {
            var factory = new CalculatorFactory();
            PageFactory.InitElements(_session, factory);
            factory.OneButton.Click();
            factory.PlusButton.Click();
            factory.TwoButton.Click();
            factory.EqualsButton.Click();
            Assert.AreEqual("3", calc.GetCalculatorResultText());
        }
        
    }
}
